**Laboratorio 2 Unidad 2**                                                                                                                            
Programa basado en metodo fork                                                                                                                                                         
# Pre-requisitos                                                                                                                                       
C++                                                                                                                                                 
Ubuntu                                                                                                                                              Make
Youtube-dl                                                                                                                                                                                                                                                                                                  
# Instalación                                                                                                                                                                                                                                                                                                                                                                                                                                      
-Instalar Make y youtube-dl si no esta en su computador.                                
Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make                                                 

Para instalar youtube-dl inicie la terminal y escriba lo siguiente:

Si tienes curl escribe este codigo:                                                                                                                  
sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl

sudo chmod a+rx /usr/local/bin/youtube-dl

Si no tienes curl escribe este  codigo:                                                                                                              
sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl

sudo chmod a+rx /usr/local/bin/youtube-dl
# Problematica

Escribir un programa el cual dada una URL de un video de youtube, lo descargue y extraiga el audio en formato MP3. Luego el programa debe ser capaz de reproducir el audio desde la terminal.
# Ejecutando

Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal: ./programa (URL del video). Luego de esto el programa empezara a descargar el video y luego reproducira el audio.

**Construido con**                                                                                                                                      
C++
# Librerias:

Iostream                                                                                                                                                
sys/wait.h                                                                                                                                              
unistd.h                                                                                                                                                

**Versionado**                                                                                                                                          
Version 1.5                                                                                                                                             
**Autores**                                                                                                                                                     
Rodrigo Valenzuela
