#include <unistd.h>
#include <sys/wait.h>
#include <iostream>

class Fork {
	private:
		pid_t pid;
		int segundos;
		char* URL;
  
	public:
		// Constructor
		Fork (int seg, char* Download) {
			URL = Download;
			segundos = seg;
			crear();
			ejecutarCodigo();
		}
	
		// Métodos.
		void crear() {
		// Se crea el proceso hijo.
		pid = fork();
		}
    
		void ejecutarCodigo () {
		// Si pid es menor que 0 el proceso hijo no se pudo crear.
		if (pid < 0) {
			std::cout << "No se pudo crear el proceso ...";
			std::cout << std::endl;
		// Si pid es igual a 0 el proceso hijo se crea
		} 
      
			else if (pid == 0) {
			std::cout << "Descargando video y extrayendo audio ...";
			std::cout << std::endl;
			// El proceso hijo ejecuta esta linea de comandos en la terminal,
			// estos comandos descargaran el video, extraeran el audio y lo pasaran a formato mp3 para su posterior reproduccion.
			execlp("youtube-dl", "-x", "--extract-audio", "--audio-format", "mp3", URL, "-o", "Audio_of_the_video.%(ext)s", NULL);
         
			sleep(segundos);
		// Se ejecuta el proceso padre
		} else {
			// Padre esperara a que el proceso hijo termine.
			wait (NULL);

			std::cout << "video y audio Descargados ...";
			std::cout << std::endl;
			// Proceso padre ejecuta estos comandos en la terminal los cuales reproduciran el audio del video con ffplay
			// este programa viene por defecto en ubuntu.
			execlp("ffplay", "nodisp", "Audio_of_the_video.mp3", NULL);
			std::cout << std::endl;
		}
	}
};

int main (int argc, char *argv[]) {
	char* URL = argv[1];
	if (URL == NULL){
		std::cout << "No ha ingresado ningun link porfavor ingrese uno" << std::endl;
	}else {
	// Instanciación.
	Fork fork(1, URL);
	}
	return 0;
}
